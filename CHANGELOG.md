* internal/config: migrate local config to `.git/glab-cli`  #813

* goreleaser: fix nfpms install dir to use /usr/bin  #817

