ARG DOCKER_VERSION=20.10.3
FROM docker:${DOCKER_VERSION}  AS docker-cli
FROM docker/buildx-bin  AS docker-buildx

FROM alpine:3.13

ARG MKDOCS_VERSION=6.1.7
ARG NGINX_VERSION=1.21
ARG HELM_VERSION=3.6.0
ARG KUBECTL_VERSION=1.21.0
ARG PORTER_VERSION=v0.33.0
ARG TERRAFORM_VERSION=0.14.11
ARG YQ_VERSION=4.11.2
ARG GLAB_VERSION=1.20.0
ARG BUILD_DATE
ARG BUILD_VERSION
ARG CONTAINER_REGISTRY
ARG CONTAINER_IMAGE
ARG CONTAINER_IMAGE_URL
ARG GIT_COMMIT_HASH
ARG GIT_COMMIT_SHORT_HASH
ARG GIT_ORIGIN_URL
ARG PROJECT_NAME
ARG PROJECT_VENDOR
ARG PROJECT_URL
ARG PROJECT_DOCS
ARG TARGET_PLATFORM
ARG TARGET_OS=linux
ARG TARGET_ARCH=amd64
ARG TARGET_VARIANT

ENV CRYPTOGRAPHY_DONT_BUILD_RUST=1
ENV PATH="$PATH:/adf/node_modules/.bin"

RUN apk add --no-cache \
    bash \
    git \
    git-fast-import \
    openssh \
    curl \
    nodejs \
    make \
    npm \
    jq

# COPY requirements.txt requirements.txt

# # Python, Ansible, etc
# RUN apk add --no-cache --virtual .build gcc musl-dev libffi-dev openssl-dev python3-dev \
#     && pip install --no-cache-dir -r requirements.txt \
#     && apk del .build gcc musl-dev \
#     && rm -rf /tmp/* /root/.cache \
#     && \
#       find ${PACKAGES} \
#         -type f \
#         -path "*/__pycache__/*" \
#         -exec rm -f {} \;

WORKDIR /adf

COPY package.json package-lock.json /adf/

RUN npm ci

COPY docker-entrypoint.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/docker-entrypoint.sh \
    && addgroup -g 1000 docker \
    && adduser -G docker -u 1000 -D docker

# Docker
COPY --from=docker-cli /usr/local/bin/docker /usr/local/bin/docker
COPY --from=docker-buildx /buildx /usr/libexec/docker/cli-plugins/docker-buildx

# Earthly
RUN curl -fsSLo /usr/local/bin/earthly "https://github.com/earthly/earthly/releases/latest/download/earthly-${TARGET_OS}-${TARGET_ARCH}" && \
    chmod +x /usr/local/bin/earthly

# Helm
RUN curl -fsSLo helm.tar.gz "https://get.helm.sh/helm-v${HELM_VERSION}-${TARGET_OS}-${TARGET_ARCH}.tar.gz" && \
    tar xfvz helm.tar.gz && \
    mv ${TARGET_OS}-${TARGET_ARCH}/helm /usr/local/bin/helm && \
    chmod +x /usr/local/bin/helm && \
    rm -rf ${TARGET_OS}-${TARGET_ARCH}

# kubectl
RUN curl -fsSLo /usr/local/bin/kubectl "https://storage.googleapis.com/kubernetes-release/release/v${KUBECTL_VERSION}/bin/${TARGET_OS}/${TARGET_ARCH}/kubectl" && \
    chmod +x /usr/local/bin/kubectl

# Terraform
RUN curl -fsSLo /tmp/terraform.zip "https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_${TARGET_OS}_${TARGET_ARCH}.zip" && \
    unzip /tmp/terraform.zip -d /usr/local/bin && \
    chmod +x /usr/local/bin/terraform && \
    rm -rf /tmp/*

# Skaffold
RUN curl -fsSLo /usr/local/bin/skaffold "https://storage.googleapis.com/skaffold/releases/latest/skaffold-${TARGET_OS}-${TARGET_ARCH}" && \
    chmod +x /usr/local/bin/skaffold

# yq
RUN curl -fsSLo /usr/local/bin/yq "https://github.com/mikefarah/yq/releases/download/v${YQ_VERSION}/yq_${TARGET_OS}_${TARGET_ARCH}" && \
    chmod +x /usr/local/bin/yq

# GitLab CLI
RUN curl -fsSLo glab.tar.gz "https://github.com/profclems/glab/releases/download/v${GLAB_VERSION}/glab_${GLAB_VERSION}_${TARGET_OS}_x86_64.tar.gz" && \
    tar xfvz glab.tar.gz && \
    mv bin/glab /usr/local/bin/glab && \
    chmod +x /usr/local/bin/glab && \
    rm -rf bin

COPY scripts/adf* /usr/local/bin/

RUN chmod +x /usr/local/bin/adf-*

ENTRYPOINT ["docker-entrypoint.sh"]

CMD ["bash"]

LABEL org.label-schema.schema-version="1.0"
LABEL org.label-schema.build-date=$APP_BUILD_DATE
LABEL org.label-schema.name=$APP_CONTAINER_IMAGE
LABEL org.label-schema.description=$APP_NAME
LABEL org.label-schema.url=$GIT_ORIGIN_URL
LABEL org.label-schema.vcs-url=$GIT_ORIGIN_URL
LABEL org.label-schema.vcs-ref=$GIT_COMMIT_HASH
LABEL org.label-schema.vcs-type="Git"
LABEL org.label-schema.vendor=$APP_AUTHOR
LABEL org.label-schema.version=$APP_BUILD_VERSION
LABEL org.label-schema.docker.dockerfile="/Dockerfile"

RUN echo "Saving image ${APP_CONTAINER_IMAGE_URL}"