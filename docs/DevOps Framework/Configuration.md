# Configuration

The application MUST be fully configurable through environment variables. It's possible to alternatively accept a configuration file, as long as this is in line with what's configurable through environment variables.
