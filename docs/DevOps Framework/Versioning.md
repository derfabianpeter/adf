# Versioning

Versions will be computed and released according to [SemVer](https://semver.org) by [semantic-release](https://semantic-release.gitbook.io/semantic-release/) in the [release stage](Lifecycle.md). A new version will be computed based on the commit messages since the last release. Commit messages must adhere to the [conventional commits](Commits.md) format to count towards the version computation.

## What versions are available

There are releases for **stable** versions that are built on the [stable branch](contribution.md) and **pre-release** versions that are built on [merge requests](Merge_requests.md).

- **stable**: v1.0.0
- **pre-release**: v1.0.0-$branch.$commit_sha

## How versions will be computed

- Commits of type `fix:` correlate with `PATCH` in [semantic versioning](https://semver.org)
  - `v1.0.0 -> v1.0.1`
- Commits of type `feat:` correlate with `MINOR` in [semantic versioning](https://semver.org)
  - `v1.0.0 -> v1.1.0`
- Commits of type `BREAKING CHANGE:` correlate with `MAJOR` in [semantic versioning](https://semver.org)
  - `v1.0.0 -> v2.0.0`
- all other commit types do not count towards new versions
