# Branches

![img](img/branches.png)

## Stable branch

There is one **stable branch** (default: `main`). The **stable branch** always contains production-ready and deployable code.

> the stable branch is also the **default** branch on services like GitHub or GitLab

## Unstable branches

**Unstable** branches are used to develop specific features. They are based on the **stable branch**. Unstable branches adhere a different naming convention on [versioning](Versioning.md) and can be used to build release-candidates of the code.

> It's not recommended to work with long-living unstable branches

After development and review, **unstable** branches will be merged back to the **stable** branch and deleted afterwards.

It's good practice to link **unstable** branches or their respective **merge requests** to issues or work items to better identify the scope of the branch.

> see [naming conventions](Naming.md) for information on branch naming

## Maintenance branches

**Maintenance branches** are based on a specifig **tag** (e.g. 1.0.0). They are used for continuous development of older releases (e.g. 1.0.x).

**Maintenance branches** are for hotfixes only. No features will be ported back to older releases.

> see [naming conventions](Naming.md) for information on branch naming