# Development workflow

## TL;DR

1. Don't work directly on the stable branch, use feature branches
2. Build and test all commits, not only on the stable branch
3. Perform code reviews before merging to the stable branch
4. Development is continuous, releases happen automatically on every merge to the stable branch
5. Development starts from the stable branch and targets the stable branch
6. Commit messages should be descriptive and must follow conventional commits

## Start a new project

1. create a [new repository](Repository.md)
2. develop your [changes](Application.md)
4. add your [documentation](Documentation.md)
5. run the [test stages](Lifecycle.md)
6. commit with a [succinct message](Commits.md)
7. push to the [remote repository](Repository.md)

> learn more about the [release workflow](Release.md)

## Change something

1. If the change requires less than 1 hour of work: start at step 3
2. Create or assign yourself an [issue](Issues.md)
3. Create a [merge request](Merge_request.md)
4. Develop your changes
5. Run the test stages
6. repeat steps 4&5 until all tests pass
7. Commit with a succinct message
8. Push to the remote repository
9. Add context to your changes in the merge request
10. When done, assign the merge request to the codeowners for review and approval
11. Repeat steps 4-10 until the merge request is approved


## For Codeowners

1. Review and approve assigned merge requests as fast as possible
2. Only merge code to the stable branch that passes all tests and guidelines
3. Handle related issues in time and communicate feedback to all relevant parties 


## General guidelines

- commit messages must adhere [Conventional Commits](Commits.md) format
- a repository must follow the [repository guidelines](Repository.md)
- the **stable** and **default** [branch](Branches.md) is `main`
- direct pushes to the **stable** branch are forbidden
- features and hotfixes will be developed in short-lived **unstable** branches
- these branches will be removed after a successful merge to the **stable** branch
- [versioning](Versioning.md) happens on all branches
- [releases](Release.md) happen only on the **stable** branch
- a CHANGELOG will be generated with every release
- releases are out of the scope of development

## FAQ

### I need to fix a critical bug in a production version. What are the steps I need to take?

- create a branch `hotfix/critical-bug` from `main`
- develop your fixes on that branch
- once done, merge `hotfix/critical-bug` into `main` and `develop` (and all relevant maintenance branches)

### I want to start working on a complex feature and it's unclear when it will be ready to be released

- create a branch `feature/awesome-stuff` from `main`
- develop your features
- create a [merge request](merge_request.md) to `main`
- wait until the feature has been selected for a release and then merge

### I finished my feature, how to bring it into the main development branch

- create a [merge request](merge_request.md) to `main`
