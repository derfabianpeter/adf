# Release workflow

A release is a snapshot of the code at the time of a specific commit. A release is typically associated with:

- a new semantic version number
- a git tag
- a GitLab release
- a GitHub release
- a docker image
- a published package in a registry
- a new version of the documentation website

Releases should happen automatically, executed by a CI/CD system or any other workflow automation engine. Typically, releases will be created after a successful [test stage](Lifecycle.md) on the [stable branch](Branches.md).

In the context of the [application lifecycle](Lifecycle.md), the [release stage](Lifecycle.md) is the last stage before the [deploy stage](Lifecycle.md). It makes sure that there are always deployable artifacts available with each increment of the application.
