# Owernship

Every application must have one or more owners. This can be the product owner or the team lead - defining the right owner is out of scope for the framework and depends on your organizational structure.

With regards to the development of the application, the owner fulfils multiple purposes:

- being the default reviewer on merge requests
- being the source of truth in terms of roadmap and scope
