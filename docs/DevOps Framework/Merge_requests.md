# Merge requests

> depending on the platform you use, Merge Requests are also known as **Pull Requests**

Merge requests (MRs) are the way you check source code changes into a branch. When you open a merge request, you can visualize and collaborate on the code changes before merge.

When [working with git](Working_with_git.md), you can use branching strategies to collaborate on code.

A repository is composed of its [default branch](Branches.md), which contains the stable version of the codebase, from which you create minor branches, also called feature branches, to propose changes to the codebase without introducing them directly into the stable version of the codebase.

Branching is especially important when collaborating with others, avoiding changes to be pushed directly to the default branch without prior reviews, tests, and approvals.

When you create a new feature branch, change the files, and push it to the repository, you have the option to create a merge request, which is essentially a request to merge one branch into another.

The branch you added your changes into is called **source branch** while the branch you request to merge your changes into is called **target branch**.

The target branch can be the default or any other branch, depending on the branching strategies you choose.

In a merge request, beyond visualizing the differences between the original content and your proposed changes, you can execute a significant number of tasks before concluding your work and merging the merge request. The **build** and **test** stages of the [devops lifecycle](Lifecycle.md) are executed for each change to a merge request to verify the integrity of the code before a merge to the stable branch can be attempted.

Merge requests are the primary way how changes to the code should happen.
