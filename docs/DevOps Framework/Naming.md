# Naming conventions

To ensure compatibility in cloud native environments and infrastructure, all things that have a **name** must follow these simple conventions:

1. Names are always in lowercase
2. Names must be convertible to DNS host names
3. Names are as short as possible

## Example

Example project: [https://gitlab.com/ayedocloudsolutions/adf](https://gitlab.com/ayedocloudsolutions/adf)

- project name:
  - example: `adf`
  - case: lowercase
  - allowed chars: `-_a-z0-9`
- author/vendor/group name:
  - example: `ayedocloudsolutions` or `ayedocloudsolutions/customers`
  - case: lowercase
  - allowed chars: `-_/a-z0-9`
- branch name:
  - example: `23-add-new-feature` or `v1.2.x`
  - description: git branches
  - case: lowercase
  - allowed chars: `-_.a-z0-9`
- image name:
  - example: `ayedocloudsolutions/adf:latest`
  - case: lowercase
  - allowed chars: `-_/:a-z0-9`
- tag:
  - description: git or docker tag
  - example: `v1.0.0` or `latest` or `d58be8b`
  - case: lowercase
  - allowed chars: `-_/.a-z0-9`
- chart name:
  - description: HELM Chart
  - example: `adf`
  - case: lowercase
  - allowed chars: `-_.a-z0-9`
- service name:
  - description: relevant in **Kubernetes** or **Docker**/**Docker Compose**
  - example: `adf-website`
  - case: lowercase
  - allowed chars: `-_a-z0-9`
- package name:
  - description: for nuget, pypi, etc
  - example: `adf`
  - case: lowercase
  - allowed chars: `-_a-z0-9`