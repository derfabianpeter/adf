# Application lifecycle

The lifecycle of an application can be split in multiple logical stages:

```yaml
plan:
develop:
build:
  image:
test:
  qa:
  unit:
  integration:
release:
  code:
  artifact:
  image:
  chart:
deploy:
  chart:
```

With every change to an application, you basically build a new application. Every change goes through the lifecycle. Development on the application happens in iterations where incremental changes will be introduced to the system.

The application can be changed through merge requests to the [stable branch](Branches.md).

> learn more about the [development workflow](Development.md)

## Plan

All changes to the application will be planned on the [roadmap](Roadmap.md). As part of the documentation the roadmap reflects multiple lifecycles of the application

## Develop

This is where the magic happens - ideas on their way to production. Work in small incremental steps and change the system frequently, but atomically rather than in fewer, bigger chunks.

## Build

An artifact will be built from the code, typically a container image.

## Test

A suite of tests will be executed on the code, the artifact and the whole application.

### Unit tests

### Integration tests

### QA tests

- Linters
- Secret Detection
- Code Quality

## Release

A new [version](Versioning.md) of the code will be released. Packages and artifacts based on the latest [version](Versioning.md) will be published.

## Deploy

A deployment of the latest [version](Versioning.md) will made to a dedicated environment.

This is typically a development, staging or review environment.
