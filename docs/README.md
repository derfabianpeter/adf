# A DevOps Framework

**A DevOps Framework** contains actionable methods and tools for DevOps-minded engineers and teams that work in product-centric high-velocity environments. It focuses on the development and operations of cloud native microservice architectures but is helpful for software development in general.

The framework should be applicable no matter what actual tools you use but to live up to its potential, a few core components work best when applying the methods and workflows of the framework:

## GitLab

While there are many solutions for version control, issue management and CI/CD, [GitLab](https://gitlab.com) has the most holistic and integrated approach to those topics and, on top, is self-hostable, which makes it a powerful tool in constrained environments (on-premise, air-gapped, cloud usage forbidden).

## Kubernetes

Kubernetes is a platform to build platforms. You can start with single-node setups and grow your way to the top along with your requirements.

Kubernetes enables predictable workflows and stable operations as well as a lot of goodies during development and the framework makes use of those features and paradigms a lot.

While it's generally possible to apply the methods of the framework to other orchestration engines (or even plain Docker), you'll miss out on many things when not using Kubernetes in this context.
